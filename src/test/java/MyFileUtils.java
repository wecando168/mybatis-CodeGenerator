
import org.apache.tomcat.util.http.fileupload.FileUtils;

import java.io.*;
import java.util.List;

/**
 * @program: demo
 * @description:
 * @author: 吴乾
 * @create: 2019-04-23 17:30
 **/
public class MyFileUtils {

    /**
     * 替换文本文件中的 非法字符串
     *
     * @param path
     * @throws IOException
     */
    public static void replacTextContent(String path, String classNmae) throws IOException {
        //原有的内容
        String srcStr = "@Table";
        //要替换的内容
        String replaceStr = "@Entity\n@Table";
        // 读
        File file = new File(path);
        FileReader in = new FileReader(file);
        BufferedReader bufIn = new BufferedReader(in);
        // 内存流, 作为临时流
        CharArrayWriter tempStream = new CharArrayWriter();
        // 替换
        String line = null;
        while ((line = bufIn.readLine()) != null) {
            // 替换每行中, 符合条件的字符串
//            添加@Entity
            if (line.indexOf("@Table") != -1) {
                line = line.replaceAll("@Table", "import java.io.Serializable;\nimport com.jspgou.common.base.domain.AbstractIdDomain;\n\n@Entity\n@Table");
            }
//             添加extends AbstractIdDomain<Integer> implements Serializable
            if (line.indexOf("public class " + classNmae) != -1) {
                line = line.replaceAll("public class " + classNmae, "public class " + classNmae + " extends AbstractIdDomain<Integer> implements Serializable");
            }

            // 将该行写入内存
            tempStream.write(line);
            // 添加换行符
            tempStream.append(System.getProperty("line.separator"));
        }
        // 关闭 输入流
        bufIn.close();
        // 将内存中的流 写入 文件
        FileWriter out = new FileWriter(file);
        tempStream.writeTo(out);
        out.close();
        System.out.println("====path:" + path);
    }

    public static void main(String[] args) throws IOException {
        replacTextContent("/Users/admin/Documents/get/demo/src/main/java/com/jspgou/frPlaybill/domain/FrPlaybill.java", "FrPlaybill");
    }
}


