package ${basePackage}.${modelNameLowerCamel}.dao;

import com.jspgou.common.base.dao.IBaseDao;
import ${basePackage}.${modelNameLowerCamel}.dao.ext.${modelNameUpperCamel}DaoExt;
import ${basePackage}.${modelNameLowerCamel}.domain.${modelNameUpperCamel};


public  interface ${modelNameUpperCamel}Dao extends IBaseDao<${modelNameUpperCamel}, Integer>, ${modelNameUpperCamel}DaoExt {
}


