package ${basePackage}.${modelNameLowerCamel}.entity;

import ${basePackage}.${modelNameLowerCamel}.entity.${modelNameUpperCamel};
import com.qitian.oa.common.web.QueryForm;

public class ${modelNameUpperCamel}Form extends QueryForm {
        ${modelNameUpperCamel}  ${modelNameLowerCamel};
}

