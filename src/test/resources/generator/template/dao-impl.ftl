package ${basePackage}.${modelNameLowerCamel}.dao.impl;

import com.jspgou.common.base.dao.BaseDao;
import ${basePackage}.${modelNameLowerCamel}.dao.ext.${modelNameUpperCamel}DaoExt;
import ${basePackage}.${modelNameLowerCamel}.domain.${modelNameUpperCamel};


public class ${modelNameUpperCamel}DaoImpl extends BaseDao<${modelNameUpperCamel}> implements ${modelNameUpperCamel}DaoExt {

}
