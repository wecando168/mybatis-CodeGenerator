package com.qitian.oa.common.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

//import com.app.web.entity.Seller;
//import com.ltimc.ywgn.system.entity.PermDepEmpM;
//import com.ltimc.ywgn.system.permMobile.entity.ImcMoRoleEmp;
//import com.ltimc.ywgn.system.permMobile.service.ImcMoRoleEmpService;
//import com.ltimc.ywgn.system.service.PermDepEmpService;

@Controller
public abstract class BaseAction {

	@Autowired


    private static String pid = "0";
    private static String mid = "0";
    public static final String CLASSES_PATH = "/WEB-INF/classes/";
	private String getPackagePath() {
		String className = this.getClass().getPackage().getName();
		String packagePath = className.replaceAll("\\.", "/");
		return CLASSES_PATH + packagePath + "/";
	}

	public ModelMap getMenu(HttpServletRequest request,ModelMap model){

		return  model;
	}

	protected String forward(String path) {

		return getPackagePath() + path;

	}

}

