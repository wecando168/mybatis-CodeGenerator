package com.qitian.oa.common.dao;


import com.qitian.oa.common.page.IPage;
import com.qitian.oa.common.web.QueryForm;

import java.util.List;

public interface IQueryDao {

    <T> List<T> list();
    <T> IPage<T> page(QueryForm form);
}
